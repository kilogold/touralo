﻿using Android.App;
using Android.OS;
using Android.Widget;

namespace Touralo
{
	[Activity (Label = "Touralo", MainLauncher = true)]
	public class Intro : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.Intro);

			var button = FindViewById<Button>(Resource.Id.btnExplore);

			button.Click += (o, e) => {
				StartActivity(typeof(MainActivity));
			};
		}
	}
}

