﻿using System;
using Android.Util;
using Android.Runtime;

namespace Touralo
{
	/// <summary>
	/// LruCache limited by memory footprint in KB rather than number of items.
	/// </summary>
	public class MemoryLimitedLruCache : LruCache
	{
		public static MemoryLimitedLruCache CreateFromTotalMemoryPercentage( float scalarPercent )
		{
			scalarPercent = scalarPercent.Clamp(0.0f, 1.0f);
			if (scalarPercent > 0.8f)
				Log.Warn ("Touralo", "LRU cache memory footprint high: " + scalarPercent * 100.0f);

			// Get max available VM memory, exceeding this amount will throw an OutOfMemory exception.
			// Stored in kilobytes as LruCache takes an int in its constructor.
			var maxMemory = (int)(Java.Lang.Runtime.GetRuntime().MaxMemory() / 1024);

			int cacheSize = (int)(maxMemory * scalarPercent);

			return new MemoryLimitedLruCache(cacheSize);
		}

		private MemoryLimitedLruCache(int size) : base(size) {}

		protected override int SizeOf(Java.Lang.Object key, Java.Lang.Object value)
		{
			// android.graphics.Bitmap.getByteCount() method isn't currently implemented in Xamarin. Invoke Java method.
			IntPtr classRef = JNIEnv.FindClass("android/graphics/Bitmap");
			var getBytesMethodHandle = JNIEnv.GetMethodID(classRef, "getByteCount", "()I");
			var byteCount = JNIEnv.CallIntMethod(value.Handle, getBytesMethodHandle);

			return byteCount / 1024;
		}
	}
}

