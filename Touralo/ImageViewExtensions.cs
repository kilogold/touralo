﻿using System;
using Android.Widget;

namespace Touralo
{
	public static class ImageViewExtensions
	{
		/// <summary>
		/// Retrieve the BitmapDownloaderTask from the ImageView's drawable.
		/// This will return null if the drawable is not a DownloadedDrawable, or there is no BitmapDownloaderTask attached to it.
		/// </summary>
		public static BitmapDownloaderTask GetBitmapDownloaderTask(this ImageView imageView)
		{
			if (imageView != null)
			{
				var drawable = imageView.Drawable as DownloadedDrawable;
				if (drawable != null)
					return drawable.GetBitmapDownloaderTask();
			}

			return null;
		}
	}
}

