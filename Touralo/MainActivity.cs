﻿using Android.App;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content.PM;
using Parse;
using System.Collections.Generic;
using System;
using Android.Graphics;
using System.Net;

namespace Touralo
{
	[Activity (ConfigurationChanges=ConfigChanges.Orientation | ConfigChanges.ScreenSize)]
	public class MainActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.EstateCatalog);

			// Load list of properties
			// Initialize the parse client with your Application ID and .NET Key found on
			// your Parse dashboard
			ParseClient.Initialize(
				"rVSHZ2zO1Vec7VFUXbn0565NX1Cl5tZse5DzrUtF",
				"mYHd9tSep2L65FhYjnj9VSaH0Y0iAUpy1W5SqJSV");

			LoadEstateList ();

		}

		[Java.Interop.Export("SelectedProperty")]
		public void SelectedProperty(View view)
		{
			TourActivity.urlToLoad = (string)view.Tag;
			StartActivity(typeof(TourActivity));
		}

		async void LoadEstateList()
		{
			var estateQuery = ParseObject.GetQuery ("Estate");
			IEnumerable<ParseObject> queryResult = await estateQuery.FindAsync();

			List<string> tourLinks = new List<string>();
			foreach (var item in queryResult) 
			{
				tourLinks.Add(item.Get<string>("TourLink"));
			}

			var customAdapter = new MainActivityAdapter (this, tourLinks.ToArray ());

			var estateListView = (ListView)FindViewById (Resource.Id.EstateListView);

			estateListView.Adapter = customAdapter;
		}
	}

	public class MainActivityAdapter : BaseAdapter<string> 
	{
        Bitmap loadingBackgroundImage;
		MemoryLimitedLruCache cache;
		string[] items;
		Activity context;

		public MainActivityAdapter(Activity context, string[] items) : base() 
		{
			this.context = context;
			this.items = items;
			cache = MemoryLimitedLruCache.CreateFromTotalMemoryPercentage(0.8f);
            loadingBackgroundImage = BitmapFactory.DecodeResource(
                Application.Context.Resources, 
                Resource.Drawable.BitmapLoading );
		}
		public override long GetItemId(int position)
		{
			return position;
		}

		public override string this[int position] 
		{  
			get { return items[position]; }
		}

		public override int Count 
		{
			get { return items.Length; }
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView; // re-use an existing view, if one is available

			if (view == null) // otherwise create a new one
				view = context.LayoutInflater.Inflate(Resource.Layout.EstateListItem, null);

			ImageButton imgBtn = view.FindViewById<ImageButton> (Resource.Id.EstateListItem_ImgButton);

			imgBtn.Tag = items [position];

			string imgPreviewUrl = items[position] + "/Preview.jpg";

			var task = new BitmapDownloaderTask(imgBtn,cache);
            var drawable = new DownloadedDrawable (task, loadingBackgroundImage);
			imgBtn.SetImageDrawable (drawable);
			task.Execute (imgPreviewUrl);

			return view;
		}


		Bitmap GetImageBitmapFromUrl(string url)
		{
			Bitmap imageBitmap = null;

			using (var webClient = new WebClient())
			{
				var imageBytes = webClient.DownloadData(url);
				if (imageBytes != null && imageBytes.Length > 0)
				{
					imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
				}
			}

			return imageBitmap;
		}
	}
}

