﻿using System;
using Android.Graphics.Drawables;
using Android.Graphics;

namespace Touralo
{
	/// <summary>
	/// A fake Drawable that will be attached to the ImageView while the download is in progress.
	/// Contains a reference to the actual download task, so that a download task can be stopped
	///  if a new binding is required, and makes sure that only the last started download process can
	///  bind its result, independently of the download finish order.
	/// </summary>
    public class DownloadedDrawable : BitmapDrawable
	{
		private readonly WeakReference<BitmapDownloaderTask> _bitmapDownloaderTaskReference;

		public DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask, Bitmap loadingBackgroundImage)
			: base(loadingBackgroundImage)
		{
			_bitmapDownloaderTaskReference = new WeakReference<BitmapDownloaderTask>(bitmapDownloaderTask);
		}

		public BitmapDownloaderTask GetBitmapDownloaderTask()
		{
			BitmapDownloaderTask task;
			_bitmapDownloaderTaskReference.TryGetTarget(out task);
			return task;
		}
	}
}

