﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Webkit;

namespace Touralo
{
	[Activity (ConfigurationChanges=ConfigChanges.Orientation | ConfigChanges.ScreenSize)]
	public class TourActivity : Activity
	{
		public static string urlToLoad = string.Empty;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.TourNavigation);
			WebView webview = FindViewById<WebView>(Resource.Id.webView1);
			webview.Settings.JavaScriptEnabled = true;
			webview.SetWebViewClient (new WebViewClient ()); // stops request going to Web Browser
			webview.LoadUrl(urlToLoad);
		}
	}
}

